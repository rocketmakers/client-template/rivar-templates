/**
 * Specify required object
 *
 * @examples require(".").sampleData
 */
export interface IModel {
  username: string;
  deletionRequestDate: string;
  displayName: string;
}

export const sampleData: IModel[] = [
  {
    username: "test@rocketmakers.com",
    deletionRequestDate: "01/01/2022",
    displayName: "Test Rockets",
  },
  {
    username: "test2@rocketmakers.com",
    deletionRequestDate: "02/01/2022",
    displayName: "Test2 Rockets",
  },
];
